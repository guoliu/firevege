\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{General Information}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Abstract and Changes}{1}{0}{1}
\beamer@sectionintoc {2}{Results}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Boreal Region}{3}{0}{2}
\beamer@subsubsectionintoc {2}{1}{1}{North America}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Tropical Region}{8}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{South America}{9}{0}{2}
\beamer@subsubsectionintoc {2}{2}{2}{Africa}{15}{0}{2}
