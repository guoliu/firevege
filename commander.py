#!/export/home/guol3/anaconda/bin/python

#import sys

# import cProfile
# import re

# outpath = '/export/home/guol3/Scripts'
# sys.path.append(outpath)
# os.chdir(outpath)

#from firevege import *
import firevege as F
reload(F)
from datetime import datetime
import os

out = 'out ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + '/'
os.makedirs(out)
print 'output folder: '+out

#tile = 'h11v02'
#veg_ty_list = ['greenNeedle','openShrub']
#fire = F.fire_read(tile)
#F.tree_freq_plt(tile,fire=fire,out=out)
#for veg_ty in veg_ty_list:
#     print tile, veg_ty
#     F.cover_ch_plt(tile,veg_ty, fire=fire,out=out)
#     F.traj_plt(tile,veg_ty,fire=fire,out=out)


#for non_tree in ['savanna','woodySavanna','grass','openShrub']:
veg_ty_list = ['greenBroad','openShrub','savanna']
print veg_ty_list
tile_list = F.tile_finder(veg_ty_list, out=out)
for tile in tile_list:
    #fire = F.fire_read(tile)
    fire = None
    F.tree_freq_plt(tile,fire=fire,out=out)
    for veg_ty in veg_ty_list:
         print tile, veg_ty
         F.cover_ch_plt(tile,veg_ty, fire=fire,out=out)
         F.traj_plt(tile,veg_ty,fire=fire,out=out)
#
#for non_tree in ['openShrub','woodySavanna','savanna','closedShrub']:
#    veg_ty_list = [non_tree, 'greenNeedle']
#    print veg_ty_list
#    tile_list = F.tile_finder(veg_ty_list, out=out)
#    for tile in tile_list:
#        #fire = F.fire_read(tile)
#        fire = None
#        F.tree_freq_plt(tile,fire=fire,out=out)
#        for veg_ty in veg_ty_list:
#            print tile, veg_ty
#            F.cover_ch_plt(tile,veg_ty, fire=fire,out=out)
#            F.traj_plt(tile,veg_ty,fire=fire,out=out)









###### flag:
########## EVI Reliability: good data v.s. marginal data 
########## Sample Pool: constant sample pixels v.s. variable sample pixels
########## Fire Season Sequence: number of 16-period selected

#for tile in vegG.keys():
#    print tile
#    fire = F.fireRead(tile)
#    for vegty in vegG[tile]:
#        print tile,vegty
#        F.fireSampPlt(tile,vegty=vegty,fire=fire)
#        F.trajPlt(tile,vegty=vegty,fire=fire)
