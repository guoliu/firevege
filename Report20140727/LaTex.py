import collections

qua_dict = {0: 'Good',
            1: 'Marginal',
            2: 'Snow/Ice',
            3: 'Cloudy'}
    
samp_dic = {0: 'Constant',
            1: 'Variable'}

## Experiment tiles

print(r'\section{Experiment Tiles}')                        
vegGroup = collections.OrderedDict({'Boreal Forest Region': {'North America': {'h12v03': ('greenNeedle','mixed')}},
                                    'Savanna Region': {'Africa': {'h20v10': ('savanna','woodySavanna')}}})

for region in vegGroup.keys():
    print(r'\subsection{' + region + '}')
    for conti in vegGroup[region].keys():
        print(r'\subsubsection{' + conti + '}')
        for tile in vegGroup[region][conti].keys():
            for vegty in vegGroup[region][conti][tile]:
                ##### Data Quality Compare
                f = 0
                q1 = 3
                q2 = 0
                S = 1
                print(r'\begin{frame}[fragile]')
                print(r'\frametitle{Comparison of different data quality: ' + region + ', ' + conti + '}')
                print(r'\framesubtitle{' + tile + ', ' + vegty + ', Fire Season Number ' + str(f+1) + '}')
                print('\n')

                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=.8\textwidth]{' + tile + vegty + 'Traj_' + str(q1) + str(S) + str(f) + '.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')

                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=.8\textwidth]{' + tile + vegty + 'Traj_' + str(q2) + str(S) + str(f) + '.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')
                
                print(r'\begin{figure}[H]')
                print(r'\includegraphics[width=0.4\linewidth]{' + tile + vegty + 'An_' + str(q1) + str(S) + str(f) + '.pdf}')
                print(r'\includegraphics[width=0.4\linewidth]{' + tile + vegty + 'An_' + str(q2) + str(S) + str(f) + '.pdf}')
                print(r'\end{figure}')
                print('\n')

                print(r'\end{frame}')
                print('\n')
                print('\n')

                ##### Sample Pool Compare
                f = 0
                q = 0
                S1 = 1
                S2 = 0
                print(r'\begin{frame}[fragile]')
                print(r'\frametitle{Comparison of different sample pool: ' + region + ', ' + conti + '}')
                print(r'\framesubtitle{' + tile + ', ' + vegty + ', ' + qua_dict[q] + ' Data, Fire Season Number ' + str(f+1) + '}')
                print('\n')

                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=.8\textwidth]{' + tile + vegty + 'Traj_' + str(q) + str(S1) + str(f) + '.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')

                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=.8\textwidth]{' + tile + vegty + 'Traj_' + str(q) + str(S2) + str(f) + '.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')
                
                print(r'\begin{figure}[H]')
                print(r'\includegraphics[width=0.4\linewidth]{' + tile + vegty + 'An_' + str(q) + str(S1) + str(f) + '.pdf}')
                print(r'\includegraphics[width=0.4\linewidth]{' + tile + vegty + 'An_' + str(q) + str(S2) + str(f) + '.pdf}')
                print(r'\end{figure}')
                print('\n')

                print(r'\end{frame}')
                print('\n')
                print('\n')


## Normal tile
#regionlist = ['Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region']
#contlist = ['Africa','Africa','Africa','Africa','South America','South America','Africa','Africa','South America','South America']
#tilelist = ['h20v10','h20v10','h20v09','h20v09','h13v10','h13v10','h21v08','h21v08','h13v09','h13v09']
#veglist = ['savanna','woodySavanna','savanna','woodySavanna','savanna','woodySavanna','greenBroad','woodySavanna','greenBroad','savanna']

print(r'\section{Other Tiles}') 
vegGroup = collections.OrderedDict({'Boreal Forest Region': {'North America': {'h12v03': ('greenNeedle','mixed'),
                                                                               'h11v02': ('greenNeedle','mixed')}},
                                    'Savanna Region': {'Africa': {'h20v10': ('savanna','woodySavanna'),
                                                                  'h20v09': ('savanna','woodySavanna')},
                                                       'South America': {'h13v10': ('savanna','woodySavanna')}},
                                    'Evergreen Broadleaf Forest-Savanna Region': {'Africa': {'h21v08': ('greenBroad','woodySavanna')},
                                                                                  'South America': {'h13v09': ('greenBroad','woodySavanna')},
                                                                                  'Australia': {'h31v10': ('greenBroad','woodySavanna')}}})

S = 1
q = 0
for region in vegGroup.keys():
    print(r'\subsection{' + region + '}')
    for conti in vegGroup[region].keys():
        print(r'\subsubsection{' + conti + '}')
        for tile in vegGroup[region][conti].keys():
            for vegty in vegGroup[region][conti][tile]:
                for f in range(0,3,2):
                    print(r'\begin{frame}[fragile]')
                    print(r'\frametitle{' + region + ', ' + conti + '}')
                    print(r'\framesubtitle{' + tile + ', ' + vegty + ', ' + qua_dict[q] + ' Data, ' + samp_dic[S] + ' Sample Size, Fire Season Number ' + str(f+1) + '}')
                    print('\n')
            
                    print(r'\begin{figure}[H]')
                    print(r'\begin{center}')
                    print(r'\includegraphics [width=0.7\textwidth]{' + tile + vegty + 'EVIcoverChan_' + str(q) + str(S) + str(f) + '.pdf}')
                    print(r'\end{center}')
                    print(r'\end{figure}')
                    print('\n')
                
                    print(r'\begin{figure}[H]')
                    print(r'\begin{center}')
                    print(r'\includegraphics [width=0.73\textwidth]{' + tile + vegty + 'Traj_' + str(q) + str(S) + str(f) + '.pdf}')
                    print(r'\end{center}')
                    print(r'\end{figure}')
                    print('\n')
                    
                    print(r'\begin{figure}[H]')
                    print(r'\includegraphics[width=0.4\linewidth]{' + tile + vegty + 'An_' + str(q) + str(S) + str(f) + '.pdf}')
                    print(r'\includegraphics[width=0.5\linewidth]{' + tile + vegty + 'fireFreq.pdf}')
                    print(r'\end{figure}')
                    print('\n')
                
                    print(r'\end{frame}')
                    print('\n')
                    print('\n')
