import scipy.io
import numpy as np
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import os

pre_raw = '/data9/randerson/guol3/CRU/RAW/pre/'
pre_geo = '/data9/randerson/guol3/CRU/precip/'
infile = pre_raw+'pre-y2009m9.mat'
outfile = pre_geo + 'pre-y2009m9.tiff'

precip = scipy.io.loadmat(infile)['cru']
nrows, ncols = np.shape(precip)

output_raster = gdal.GetDriverByName('GTiff').Create(outfile, ncols, nrows, 1 ,gdal.GDT_Float32, options = ['COMPRESS=LZW'])  
# ouput file name, the raster dimensions, the number of layers, the data type of the raster. 

## Set coordinates
geotransform=(-180, .5, 0, 90, 0, -.5) #    
# top left x, w-e pixel resolution, rotation (0 if North is up), 
# top left y, rotation (0 if North is up), n-s pixel resolution
output_raster.SetGeoTransform(geotransform)  # Specify its coordinates

## Set projection
srs = osr.SpatialReference()                 # Establish its coordinate encoding
srs.ImportFromEPSG(4326)                     # This one specifies WGS84 lat long.
output_raster.SetProjection( srs.ExportToWkt() )   # Exports the coordinate system to the file
output_raster.GetRasterBand(1).WriteArray(precip)   # Writes my array to the raster

del output_raster

os.system('gdalwarp %s %s -t_srs "+proj=sinu +R=6371007.181 +nadgrids=@null +wktext"' %(outfile, pre_geo+'MODIS_proj/pre-y2009m9.tiff'))

##########
import os

dst = '/data9/randerson/guol3/Africa/'
os.system('modis_mosaic.py -m "/export/home/guol3/local/MRT/" -o %s %s' %(outfile1, tilelist))
os.system('odis_convert.py -m "/export/home/guol3/local/MRT/" -o %s -g 500 %s' %(outfile2, outfile1))
