import os
import numpy as np
from pymodis import convertmodis

dst = '/data9/randerson/guol3/Africa/'
outfile1 = dst + 'MCD12Q1.mod.Afr.hdf'
outfile2 = dst +'MCD12Q1.geo.Afr.hdf'

h_l = np.arange(16,24)
v_l = np.arange(5,12)

txtflnm = 'Africa_tile.txt'
tilelist = '\n'.join(cover_path+'MCD12Q1.A'+str(year)+'001.h'+str(h).zfill(2)+'v'+str(v).zfill(2)+'.051.hdf' for h in h_l for v in v_l)
tiletxt = open(txtflnm, 'a')
tiletxt.write(tiletxt)
tiletxt.close

for year in range(2001,2002):
    cover_path = '/gdata/randerson2/group/MODIS/MCD12Q1/V051/' + str(year) + '.01.01/'
    os.system('modis_mosaic.py -m "/export/home/guol3/local/MRT/" -o %s %s' %(outfile1, txtflnm))
    #os.system('modis_convert.py -m "/export/home/guol3/local/MRT/" -o %s -g 500 %s' %(outfile2, outfile1))
