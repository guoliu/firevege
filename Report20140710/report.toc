\contentsline {section}{\tocsection {}{1}{Introduction}}{1}{section.1}
\contentsline {section}{\tocsection {}{2}{Boreal Forest Region}}{3}{section.2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Alaska}}{3}{subsection.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{2.1.1}{Tile h11v02}}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\tocsubsubsection {}{2.1.2}{Tile h10v02}}{5}{subsubsection.2.1.2}
\contentsline {subsection}{\tocsubsection {}{2.2}{Eurasia}}{7}{subsection.2.2}
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.1}{Evergreen Needleleaf Forest}}{7}{subsubsection.2.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.2}{Mixed Forest}}{8}{subsubsection.2.2.2}
\contentsline {section}{\tocsection {}{3}{Savanna Region}}{9}{section.3}
\contentsline {subsection}{\tocsubsection {}{3.1}{Africa}}{9}{subsection.3.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.1.1}{Savanna}}{9}{subsubsection.3.1.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.1.2}{Woody Savanna}}{10}{subsubsection.3.1.2}
\contentsline {subsection}{\tocsubsection {}{3.2}{South America}}{11}{subsection.3.2}
\contentsline {subsubsection}{\tocsubsubsection {}{3.2.1}{Savanna}}{11}{subsubsection.3.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.2.2}{Woody Savanna}}{12}{subsubsection.3.2.2}
\contentsline {subsection}{\tocsubsection {}{3.3}{Australia}}{13}{subsection.3.3}
\contentsline {subsubsection}{\tocsubsubsection {}{3.3.1}{Savanna}}{13}{subsubsection.3.3.1}
\contentsline {subsubsection}{\tocsubsubsection {}{3.3.2}{Woody Savanna}}{14}{subsubsection.3.3.2}
\contentsline {section}{\tocsection {}{4}{Evergreen Broadleaf Forest - Savanna Region}}{15}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{Africa}}{15}{subsection.4.1}
\contentsline {subsubsection}{\tocsubsubsection {}{4.1.1}{Evergreen Broadleaf Forest}}{15}{subsubsection.4.1.1}
\contentsline {subsubsection}{\tocsubsubsection {}{4.1.2}{Woody Savanna}}{16}{subsubsection.4.1.2}
\contentsline {subsection}{\tocsubsection {}{4.2}{South America}}{17}{subsection.4.2}
\contentsline {subsubsection}{\tocsubsubsection {}{4.2.1}{Evergreen Broadleaf Forest}}{17}{subsubsection.4.2.1}
\contentsline {subsubsection}{\tocsubsubsection {}{4.2.2}{Savanna}}{18}{subsubsection.4.2.2}
