\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{General Information}{1}{0}{1}
\beamer@subsectionintoc {1}{1}{Tiles and Changes}{1}{0}{1}
\beamer@sectionintoc {2}{Experiment Tiles}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Boreal Forest Region}{3}{0}{2}
\beamer@subsubsectionintoc {2}{1}{1}{North America}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Savanna Region}{8}{0}{2}
\beamer@subsubsectionintoc {2}{2}{1}{Africa}{9}{0}{2}
\beamer@sectionintoc {3}{Other Tiles}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Boreal Forest Region}{13}{0}{3}
\beamer@subsubsectionintoc {3}{1}{1}{North America}{14}{0}{3}
\beamer@subsectionintoc {3}{2}{Savanna Region}{22}{0}{3}
\beamer@subsubsectionintoc {3}{2}{1}{South America}{23}{0}{3}
\beamer@subsubsectionintoc {3}{2}{2}{Africa}{27}{0}{3}
\beamer@subsectionintoc {3}{3}{Evergreen Broadleaf Forest-Savanna Region}{35}{0}{3}
\beamer@subsubsectionintoc {3}{3}{1}{South America}{36}{0}{3}
