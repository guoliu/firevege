import os, datetime, glob, random, sys, math, socket, numpy as np, numpy.ma as ma,  matplotlib as mpl
from osgeo import gdal
from dateutil.relativedelta import relativedelta

vegdic = {'water': 0,
          'greenNeedle': 1,
          'greenBroad': 2,
          'deciNeedle': 3,
          'deciBroad': 4,
          'mixed': 5,
          'closedShrub': 6,
          'openShrub': 7,
          'woodySavanna': 8,
          'savanna': 9,
          'grass': 10,
          'wetland': 11,
          'crop': 12,
          'urban': 13,
          'mosaic': 14,
          'cryo': 15,
          'barren': 16}

startY = 2000
endY = 2014
annuN = 365//16+1
preLen_min = np.floor(1.5*annuN)

workPath = '/gdata/randerson/guol3/'

fireRaw = '/export/gdata/randerson2/giglio/MCD64A1/'
firePath = workPath + 'MCD64A1/'

EVIRaw = '/gdata/randerson2/group/MODIS/MOD13A1-by-tile/'
EVIPath = workPath + 'MOD13A1/'

treeRaw = '/gdata/randerson3/group/MODIS/MOD44B-by-tile/V005/'
treePath = workPath + 'MOD44B/'

coverRaw = '/gdata/randerson2/group/MODIS/MCD12Q1/V051/'
coverPath = workPath + 'MCD12Q1/'

if socket.gethostname() == 'Lycopodium':
    localPath = '/Volumes/LycopodiumHDrive/Research/Data/'
    firePath2 = localPath + 'MCD64A1/'
    EVIPath2 = localPath + 'MOD13A1/'
    coverPath2 = localPath + 'MCD12Q1/'
    os.system('scp guol3@gplogin1.ps.uci.edu:' + firePath + tile + '/* ' + firePath2 + tile)
    os.system('scp guol3@gplogin1.ps.uci.edu:' + EVIPath + tile + '/* ' + EVIPath2 + tile)
    os.system('scp guol3@gplogin1.ps.uci.edu:' + coverPath + tile + '/* ' + coverPath2 + tile)
    firePath = firePath2
    EVIPath = EVIPath2
    coverPath = coverPath2

####################################################################################################
def daterange16(start, end):
    return (datetime.date(y,1,1)+datetime.timedelta(d-1) for y in range(start, end) for d in range(1,366,16))

####################################################################################################
def daterangem(start, end):
    return (datetime.date(y, m, 1) for y in range(start, end) for m in range(1, 13))

####################################################################################################
def rebin(a, shape):
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).mean(-1).mean(1)

####################################################################################################
def tileCord(tile):
    cPath = os.path.dirname(os.path.realpath(sys.argv[0]))
    tileList = np.loadtxt('tilelist.txt')

    lonDic = {-1: 'W',
               0: '',
               1: 'E'}

    latDic = {-1: 'S',
               0: '',
               1: 'N'}

    lonN = tileList[(tileList[:,0]==np.int(tile[1:3]))&(tileList[:,1]==np.int(tile[4:6])),2][0]
    lon = np.str(np.absolute(lonN))+lonDic[np.sign(lonN)]
    
    latN = tileList[(tileList[:,0]==np.int(tile[1:3]))&(tileList[:,1]==np.int(tile[4:6])),3][0]
    lat = np.str(np.absolute(latN))+latDic[np.sign(latN)]

    return lat+','+lon
####################################################################################################
def fireRead(tile):
## Data Transfer
    src = fireRaw + tile + '/'
    dst = firePath + tile  + '/'
    if not os.path.exists(dst):
        print 'Fire data is not ready. Trying to prepare data now...'
        os.makedirs(dst)
    open(tile + 'firemissing.txt', 'w').close()

    fire = np.ones((2400, 2400, (endY - startY) * annuN), dtype=np.bool)
    l = 0
    for dp in daterange16(startY, endY):
        f = 0 #flag of fire data existence
        flnm_n = dst + 'MCD64A1.' + dp.strftime('%Y') + dp.strftime('%j') + tile + '.npy' 
        if os.path.isfile(flnm_n): #data already transformed 
            fire[..., l] = np.load(flnm_n)
        else: #data not transformed or missing
            d = [datetime.date(dp.year,dp.month,1), datetime.date(dp.year,dp.month,1) - relativedelta(months=1)]
            fire_p = np.empty((2400, 2400, 2), dtype=np.int) # 0: present; 1: previous
            for p in range(2):
                flnm = glob.glob(src + 'MCD64A1.A' + d[p].strftime('%Y') + d[p].strftime('%j') + '.' + tile + '.051.*.hdf')
                if flnm:                        
                    temp = gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm[0] + '":MOD_Grid_Monthly_500m_DB_BA:Burn Date', gdal.GA_ReadOnly).ReadAsArray()
                    fire_p[:, :, p] = (datetime.date(d[p].year, 1, 1).toordinal() + temp - 1).astype(int)
                    fire_p[temp <= 0, p] = 0
                else:
                    f += 1
            if f != 2: #data not transformed
                fire_mask = np.logical_or((fire_p[...,0] <= dp.toordinal()) & (fire_p[...,0] > dp.toordinal()-16), fire_p[...,1] > dp.toordinal()-16)
                fire[..., l] = fire_mask
                np.save(flnm_n, fire_mask)
                print dp
            else: #data missing
                myfile = open(tile + 'firemissing.txt', 'a')
                myfile.write('No fire data for ' + dp.strftime('%Y') + ',' + dp.strftime('%j') + ';\n')
                myfile.close()
        l += 1
    return fire

####################################################################################################
#0   Good Data          Use with confidence
#1   Marginal data      Useful, but look at other QA information
#2   Snow/Ice           Target covered with snow/ice
#3   Cloudy             Target not visible, covered with cloud
def EVIRead(tile, qua = 0): 
    src = EVIRaw + tile + '/' #2000049-2013305
    dst = EVIPath + tile + '/'
    flnm = glob.glob(src + 'MOD13A1.*' + tile + '*.hdf')
    if not os.path.exists(dst):
        os.makedirs(dst)
    for p in range(len(flnm)):
        flnm_nE = dst + 'MOD13A1.' + flnm[p][-18:-11] + tile + 'EVI.npy'
        flnm_nN = dst + 'MOD13A1.' + flnm[p][-18:-11] + tile + 'NBR.npy'
        if not os.path.isfile(flnm_nN):
            print 'Transforming EVI and NBR data ' + flnm[p][-18:-11] + tile + '...'
            relia = gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm[p] + '":MODIS_Grid_16DAY_500m_VI:500m 16 days pixel reliability', gdal.GA_ReadOnly).ReadAsArray().astype(int)
            reliaM = (relia>=0)&(relia<=qua)
            temp = gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm[p] + '":MODIS_Grid_16DAY_500m_VI:500m 16 days EVI', gdal.GA_ReadOnly).ReadAsArray()
            EVI = np.float64(temp) * 0.0001
            EVI[reliaM] = np.nan
            np.save(flnm_nE, EVI)
            temp = np.dstack((gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm[p] + '":MODIS_Grid_16DAY_500m_VI:500m 16 days NIR reflectance', gdal.GA_ReadOnly).ReadAsArray(), gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm[p] + '":MODIS_Grid_16DAY_500m_VI:500m 16 days MIR reflectance', gdal.GA_ReadOnly).ReadAsArray()))
            temp = np.float16(temp) * 0.0001
            temp[reliaM,:] = np.nan 
            NBR = (temp[..., 0] - temp[..., 1]) / np.sum(temp, axis=2)
            np.save(flnm_nN, NBR)

####################################################################################################
def treeRead(tile, y = 2001):
    src = treeRaw + tile + '/'
    flnm = src + 'MOD44B.A' + str(y) + '065.' + tile + '.005.hdf'
    
    dst = treePath + tile + '/'
    flnm_n = dst + 'MOD44B.' + str(y) + tile + '.npy' 
    #if os.path.isfile(flnm_n):
    #    return np.load(flnm_n)
    
    if not os.path.exists(dst):
        os.makedirs(dst)
    if os.path.isfile(flnm):
        temp = gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm + '":MOD44B_250m_GRID:Percent_Tree_Cover', gdal.GA_ReadOnly).ReadAsArray().astype(int)     
        tree = rebin(ma.masked_where(temp>=200, temp), [2400,2400])
        #np.save(flnm_n, tree)
    else:
        myfile = open(tile + 'treemissing.txt', 'a')
        myfile.write(flnm + '; occurred for 0 times.\n')
        myfile.close
        tree = []
        print 'Where is my tree cover?!'
    return tree 

####################################################################################################
def coverRead(tile, y = 2001):
    src = coverRaw + str(y) + '.01.01/'
    flnm = src + 'MCD12Q1.A' + str(y) + '001.' + tile + '.051.hdf'
    
    dst = coverPath + tile + '/'
    flnm_n = dst + 'MCD12Q1.' + str(y) + tile + '.npy'
    if os.path.isfile(flnm_n):
        return np.load(flnm_n)

    if not os.path.exists(dst):
        os.makedirs(dst)
    if os.path.isfile(flnm):
        cover = gdal.Open('HDF4_EOS:EOS_GRID:"' + flnm + '":MOD12Q1:Land_Cover_Type_1', gdal.GA_ReadOnly).ReadAsArray()     
        np.save(flnm_n, cover)
    else:
        myfile = open(tile + 'covermissing.txt', 'a')
        myfile.write(flnm + '; occurred for 0 times.\n')
        myfile.close
        cover = []
        print 'Where is my land cover?!'
    return cover

####################################################################################################
def dTS(NBR, startM = 14):
    step = 23
    aNBR = NBR[..., range(startM, startM + step * 14, step)]
    butt = np.empty((ind[1] - ind[0], ind[3] - ind[2]))
    butt[:] = np.nan
    dNBR = aNBR - np.dstack((butt, aNBR[..., :-1]))
    x_v = []
    for dp in daterange16(2000, 2014):
        x_v = np.append(x_v, dp.toordinal())

    x_d = x_v[range(startM, step * 14, step)]
    return (dNBR, x_d)

####################################################################################################
def fireSamp(tile, vegty = 'savanna', fire=None):
    if os.path.isfile(tile + vegty+ 'Samp.npz'):
        with np.load(tile + vegty+ 'Samp.npz') as data:
            subfire = data['subfire']
            subEVI = data['subEVI']
            subNBR = data['subNBR']
            s = data['s']
        return subfire, subEVI, subNBR, s
    
    vegNum = vegdic[vegty]
    direF = firePath + tile + '/'
    direV = EVIPath + tile + '/'
    if not os.path.exists(direV):
        print 'Vegetation data is not ready. Tring to prepare data now...'
        EVIRead(tile)
    if fire is None:
        fire = fireRead(tile)
    
    s = 0 # Start date number of data. Assuming fire data starts later than vegetation data.
    while fire[...,s].all(): 
        s += 1

    fireM = np.any(fire[...,s:], axis=2)
    cover = coverRead(tile)

    coverM = cover == vegNum
    pool = np.array(np.nonzero(np.logical_and(fireM, coverM))).T
    if np.size(pool,0) < 9:
        ind = pool
    else:
        ind = np.array(random.sample(pool, 8))
    print 'Available sample #:',np.size(pool,0)
    if np.size(pool,0) == 0:
        print 'Sample size too small.'
        print 'Percentage of', vegty, ':', coverM.sum()*1.0/(cover<17).sum()
        print 'Total number of fire events:', fire.sum()
        print 'Number of pixles burnt on current vegetation type:', (fireM&coverM).sum()

    subfire = fire[ind[:, 0], ind[:, 1], :]
    del fire
    print 'Reading vegetation data according to mask now...'
    open(tile + 'EVImissing.txt', 'w').close()
    subEVI = np.zeros((np.size(ind, 0), (endY - startY) * annuN))
    subNBR = np.zeros((np.size(ind, 0), (endY - startY) * annuN))
    subEVI[:] = np.nan
    subNBR[:] = np.nan
    l = 0
    for dp in daterange16(startY, endY):
        flnmE = direV + 'MOD13A1.' + dp.strftime('%Y') + dp.strftime('%j') + tile + 'EVI.npy'
        flnmN = direV + 'MOD13A1.' + dp.strftime('%Y') + dp.strftime('%j') + tile + 'NBR.npy'
        if os.path.isfile(flnmE):
            subEVI[..., l] = np.load(flnmE)[ind[:, 0], ind[:, 1]]
            subNBR[..., l] = np.load(flnmN)[ind[:, 0], ind[:, 1]]
        else:
            myfile = open(tile + 'EVImissing.txt', 'a')
            myfile.write(dp.strftime('%Y') + dp.strftime('%j') + tile + '; occurred for ' + '0 times.\n')
            myfile.close()
        l += 1

    np.savez(tile + vegty+ 'Samp.npz', subEVI = subEVI, subNBR = subNBR, subfire = subfire, s=s)
    return subfire, subEVI, subNBR, s
####################################################################################################
def fireSampPlt(tile, vegty = 'savanna', fire = None):
    subfire, subEVI, subNBR, s = fireSamp(tile, vegty, fire)

    ### x axis of vegetation time series
    x_v = []
    for dp in daterange16(startY, endY):
        x_v = np.append(x_v, dp.toordinal())
    x_v = x_v[s:]
    
    ### ticks and labels on x axis
    xt = []
    xl = []
    k = 0
    for y in range(startY + 1, endY):
        xt = np.append(xt, datetime.date(y, 1, 1).toordinal())
        if k%((endY - startY) / 3) == 0:
            xl = np.append(xl, str(y))
        else:
            xl = np.append(xl, '')
        k += 1

### Plotting
    print 'Plotting samples now...'
    mpl.use('PDF')
    import matplotlib.pyplot as plt, prettyplotlib as ppl

    fig = plt.figure()
    f = []
    E = []
    N = []
    for p in range(np.size(subEVI,0)):
        fireSeri = subfire[p, :]
        plt.subplot(4, 2, p)
        k = 0
        for dp in daterange16(startY,endY):
            if fireSeri[k]&(k>=s):
                f = plt.axvline((dp-datetime.timedelta(days=8)).toordinal(), color='yellow', linewidth=1.5, zorder=0)
            k += 1
        E, = ppl.plot(x_v, subEVI[p, s:], color='blue', linewidth=0.9)
        N, = ppl.plot(x_v, subNBR[p, s:], color='black', linewidth=0.3)
        plt.xticks(xt, xl, size='small')
        plt.locator_params(axis='y', nbins=5)
        plt.xlim(x_v[0],x_v[-1])
    fig.suptitle('Samples from ' + vegty + ' @' + tileCord(tile), fontsize=14)
    leg = fig.legend((f, E, N), ('fire', 'EVI', 'NBR'), loc='lower center', ncol=3, shadow=True)
    leg.draw_frame(False)
    plt.savefig(tile + vegty + 'EVISamp' + '.pdf', dpi=300)
    plt.close()

####################################################################################################
def coverHisPlt(tile, cover=None):
    if cover is None:
        cover = coverRead(tile)
    
    if os.path.isfile(tile+'coverHis.pdf'):
        return

    his = np.zeros((17,), dtype=np.int)
    for vegNum in range(17):
        his[vegNum] = (cover==vegNum).sum()
    
    mpl.use('PDF')
    import matplotlib.pyplot as plt, prettyplotlib as ppl
    plt.figure(figsize=(11, 6), dpi=300) 
    ppl.bar(range(17), his, width = 1)
    plt.xticks(range(17),sorted(vegdic , key=vegdic.get), rotation=30,  fontsize=14)
    plt.ylabel('Pixel Number',  fontsize=14)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.title('Vegetation Type ' + ' @' + tileCord(tile), fontsize=16)
    plt.tight_layout() 
    plt.savefig(tile+'coverHis.pdf', dpi=300)
    plt.close()

####################################################################################################
def fireFreq(tile, vegty = 'savanna', fire = None):
    if os.path.isfile(tile+vegty+'Seaso.npy'):
        seaso = np.load(tile+vegty+'Seaso.npy')
        return seaso

    if fire is None:
        fire = fireRead(tile)
    vegNum = vegdic[vegty]
    cover = coverRead(tile)
    coverM = cover == vegNum
    coverHisPlt(tile, cover=cover)
    del cover

    fireMa = fire&np.tile(coverM.reshape(2400,2400,1), (1,1,np.size(fire,2)))
    freq = np.sum(fireMa[...,annuN:].reshape((2400, 2400, -1, annuN)), axis=2)    
    freq_t = freq.sum(axis=1).sum(axis=0).flatten()#*1.0/np.sum(coverM)/(endY-startY)
    if not freq_t.any():
        print 'Sample size too small.'
        print 'Number of fire events on', vegty, ':', fireMa.sum()
        return -1
    del freq

    mpl.use('PDF')
    import matplotlib.pyplot as plt, prettyplotlib as ppl
  
    x_a = []
    for dp in daterange16(startY, startY+1):
        x_a = np.append(x_a, (dp-datetime.timedelta(days=8)).toordinal())
    
    xt = []
    for m in range(1, 13):
        xt = np.append(xt, datetime.date(startY, m, 1).toordinal())
    
    plt.figure(figsize=(11, 6), dpi=300)
    ppl.bar(x_a, freq_t, width = 16)
    plt.xticks(xt, ('Jan', '', 'Mar', '', 'May', '', 'Jul', '', 'Sep', '', 'Nov', ''))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ylabel('Burning Frequency')
    plt.suptitle('Fire Seasonality of ' + vegty + ' @' + tileCord(tile), fontsize=16)
    plt.savefig(tile + vegty + 'fireFreq' + '.pdf', dpi=300)
    plt.close()

    seaso = (freq_t == np.amax(freq_t)).nonzero()[0][0]
    np.save(tile+vegty+'Seaso.npy', seaso)
    return seaso

####################################################################################################
## Calculate post-fire trajectory of EVI and NBR. Will transform EVI, NBR, fire and landcover data if not already transformed. Also calls fireFreq to plot fire frequecy of each month.
## Input: 
###### tile number and vegetation type (default as 'savanna').
## Output:
###### 1. histogram of sample size vs. time series length; 2. mean, upper 10% and lower 10% of EVI and NBR; 3. month of fire seaso. EVI and NBR include one year of pre-fire data in the beginning. Output is saved as npz file.
## Dependecy:
###### EVIRead, fireRead, fireFreq, coverRead, daterange16

def trajCal(tile, vegty = 'savanna', fire = None):
    if os.path.isfile(tile + vegty+'Traj.npz'):
        with np.load(tile + vegty+'Traj.npz') as data:
            preLen = data['preLen']
            s = data['s']
            his = data['his']
            mea = data['mea']
            upp = data['upp']
            low = data['low']
            colA = data['colA']
        return preLen, s, his, mea, upp, low, colA

    if not os.path.exists(EVIPath + tile + '/'):
        EVIRead(tile)
### Prepare data
    if fire is None:
        fire = fireRead(tile)
    
    print 'Calculating post-fire trajectory...'
    ## Start date of data. Assuming fire data starts later than vegetation data.
    s = 0 # Start date number of data. Assuming fire data starts later than vegetation data.
    while fire[...,s].all(): 
        s += 1
    
    seaso = fireFreq(tile, vegty=vegty, fire=fire)
    preLen = np.int(((preLen_min-(seaso-s))//annuN+1)*annuN+(seaso-s))  #Length of pre-fire time-series
    print 'Length of pre-fire time-series:', preLen

    vegNum = vegdic[vegty]
    cover = coverRead(tile)
    coverM = cover == vegNum
    del cover
    
    ## Initialize data
    rowN = np.sum(np.any(fire[:, :, s+preLen::annuN],axis=2)&coverM)
    if not (s+preLen)%annuN==seaso:
        print 'preLen wrong again, haha!!!'
        return 0

    print 'Estimated sample size: < ' + str(rowN) + '. Scanning through layers...'    
    if rowN==0:
        print 'Sample size too small.'
        return 0

    fireTotal = np.empty([2, rowN, (endY - startY) * annuN - s])
    fireTotal[:] = np.nan
    colA = -np.ones((2400, 2400), dtype=np.int)
    rowA = -np.ones((2400, 2400), dtype=np.int)
    recM = np.zeros((2400, 2400), dtype=np.bool) #Pixels alive.
    EVItem = np.empty([2400, 2400, preLen]) #EVI history data for preLen
    NBRtem = np.empty([2400, 2400, preLen]) #NBR history data for preLen 

    open(tile + 'EVImissing.txt', 'w').close()
    print 'Scanning through layers...'
    l = 0
    for dp in daterange16(startY, endY):
        print dp
        flnmE = EVIPath + tile + '/MOD13A1.' + dp.strftime('%Y') + dp.strftime('%j') + tile + 'EVI.npy'
        flnmN = EVIPath + tile + '/MOD13A1.' + dp.strftime('%Y') + dp.strftime('%j') + tile + 'NBR.npy'
        if os.path.isfile(flnmE): 
            fire_c = fire[..., l]
            burnMa = fire_c & coverM
            recM[burnMa] = False #Pixel killed 
            colA[recM] += 1 #Alive pixels
            if (l%annuN == seaso)&(l>=(s+preLen)): # Fire seaso
                preM = np.sum(fire[:, :, l-preLen:l], axis=2) == 0 #Didn't burn for preLen 16-days
                newPxl = burnMa & preM & (colA==-1) #New born pixels, never lived before
                if newPxl.any():
                    ## Creat new samples and update row and column
                    recM[newPxl] = True #Pixels alive
                    rowA[newPxl] = np.arange(np.sum(newPxl)) + np.amax(rowA) + 1 #Start new space for new born
                    colA[newPxl] = preLen #Start to record as the preLen-th 16-day 
                    ## Load pre-fire EVI and NBR
                    fireTotal[0, rowA[newPxl], 0:preLen] = np.roll(EVItem,preLen-(l-1)%preLen-1,axis=2)[newPxl, :]
                    fireTotal[1, rowA[newPxl], 0:preLen] = np.roll(NBRtem,preLen-(l-1)%preLen-1,axis=2)[newPxl, :]
            EVItem[..., l%preLen] = np.load(flnmE)
            NBRtem[..., l%preLen] = np.load(flnmN)
            fireTotal[0, rowA[recM], colA[recM]] = EVItem[recM, l%preLen]
            fireTotal[1, rowA[recM], colA[recM]] = NBRtem[recM, l%preLen]
        else: #Missing EVI data
            myfile = open(tile + 'EVImissing.txt', 'a')
            myfile.write(dp.strftime('%Y') + dp.strftime('%j') + tile + '\n')
            myfile.close()
        l += 1

    his = np.sum(~np.isnan(fireTotal[0,:,:]),axis = 0)    
    mea = np.nanmean(fireTotal, axis=1)
    upp = np.empty([2, fireTotal.shape[2]])
    upp[:] = np.nan
    low = np.empty([2, fireTotal.shape[2]])
    low[:] = np.nan
    
    for p in range(fireTotal.shape[2]):
        for layer in range(2):
            data_temp = fireTotal[layer, :, p]
            mask_temp = ~np.isnan(data_temp)
            if mask_temp.any():
                upp[layer, p] = np.percentile(data_temp[mask_temp], 90)
                low[layer, p] = np.percentile(data_temp[mask_temp], 10)

    np.savez(tile + vegty+'Traj.npz', preLen=preLen, s=s, his=his, mea=mea, upp=upp, low=low, colA=colA)
    return preLen, s, his, mea, upp, low, colA

####################################################################################################
def trajMap(tile, vegty = 'savanna', fire=None):
    preLen, s, _, _, _, _, colA = trajCal(tile, vegty=vegty, fire = fire)
    tree = treeRead(tile)
    cover = coverRead(tile)
    coverM= ~(cover==vegdic[vegty])
    #tree.data[coverM]=-100

    mpl.use('PDF')
    import matplotlib.pyplot as plt
    from matplotlib import colors

    fig, axes = plt.subplots(nrows=2, ncols=7, figsize=(12, 5), dpi=400)
    plt.subplots_adjust(left = .01, bottom = .1, right = .99, top = .8, wspace = 0, hspace = 0.3)
    
    m_cmap = mpl.cm.YlGn
    m_cmap.set_bad(color='k')
    #m_cmap.set_under(color='b')
    
    for y in range(0,13):
        samp = np.nonzero(colA>preLen+(y-1)*annuN)  
        im = axes.flat[y].imshow(tree, origin='upper', interpolation='nearest', cmap=m_cmap, clim=(0,100))
        axes.flat[y].scatter(samp[1], samp[0] , s=2, c='r', alpha=.2, edgecolor = 'none')
        axes.flat[y].set_title(str(y), fontsize=14)
        axes.flat[y].axis('off')
    cax, kw = mpl.colorbar.make_axes([ax for ax in axes.flat[:7]], location='top', shrink=.8, aspect=50, pad=0.15)
    cbar = plt.colorbar(im, cax=cax, **kw)
    cbar.solids.set_edgecolor("face")

    c_cmap = plt.cm.Set3
    bounds = np.array(range(-1,17))+.5
    norm = colors.BoundaryNorm(bounds, c_cmap.N)
    #c_cmap.set_bad(color='k')

    im = axes.flat[-1].imshow(cover, origin='upper', interpolation='nearest', cmap=c_cmap, norm=norm)
    axes.flat[-1].axis('off')
    axes.flat[-1].set_title('Vegetation Type', fontsize=14)
    cax, kw = mpl.colorbar.make_axes([ax for ax in axes.flat[6:]], location='bottom', shrink=.8, aspect=50, pad=0.01)
    cbar = plt.colorbar(im, cax=cax, norm=norm, ticks=range(17), **kw)
    cbar.solids.set_edgecolor("face")
    cbar.ax.set_xticklabels(sorted(vegdic, key=vegdic.get), rotation=15, fontsize=14)


    fig.suptitle('Sample change ' + vegty + ' @' + tileCord(tile), fontsize=20)
    plt.savefig(tile + vegty + 'trajMap' + '.pdf', dpi=400)
    plt.close()

####################################################################################################
## Plot post-fire trajectory using result from function trajCal, in PDF format. Will call trajCal if required data is not found.
## Input:
###### tile number and vegetation type.
## Output: 
###### 1. Histogram of sample size; 2. Post-fire trajectory of EVI and NBR.
## Dependency:
###### trajCal, daterange16

def trajPlt(tile, vegty = 'savanna', fire=None):
    preLen,s,his,mea,upp,low,_ = trajCal(tile, vegty=vegty, fire = fire)
    
    mpl.use('PDF')
    import matplotlib.pyplot as plt 

    i = 0
    x_v = [] # x-axis
    for dp in daterange16(startY, endY):
        x_v = np.append(x_v, dp.toordinal())
        i += 1
    x_v = x_v[s:]
    fire_d = datetime.date.fromordinal(np.int(x_v[preLen]-8)) #Center of 16-day period
    
    xt = [] # x tick
    xl = [] # x label
    f_tick = fire_d-relativedelta(years=2)
    for y in range(startY-2, endY):
        xt = np.append(xt, f_tick.toordinal())
        xl = np.append(xl, str(y - startY))
        f_tick += relativedelta(years=1)

    print 'Plotting trajectory now...'
    subtit = ['EVI', 'NBR']
    fig = plt.figure()
    for p in range(2):
        ax = plt.subplot(2, 1, p+1)
        A = plt.fill_between(x_v, upp[p, :], low[p, :], linewidth=0, alpha=.2)
        B = plt.axvline(fire_d.toordinal(), color='yellow', linewidth=1.5, zorder=0)
        M, = plt.plot(x_v, mea[p, :], color='black', linewidth=0.5)
        plt.xticks(xt, xl, size='small')
        plt.ylabel(subtit[p])
        ax.yaxis.grid(b=True, alpha=.2)
        if p == 0: 
            U = plt.axhline(y=np.max(mea[p, 0:annuN-1]), color='black', linewidth=.7, alpha=.3)
            L = plt.axhline(y=np.min(mea[p, 0:annuN-1]), color='black', linewidth=.7, alpha=.3)
            plt.xlabel('Time since fire (year)')
        else:
            ax2 = ax.twinx()
            S, = plt.plot(x_v, his, linewidth=2,  color='black', alpha = .3)
            plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            plt.ylabel('Sample Size')
        plt.xlim(x_v[0],x_v[-1])
    ax.xaxis.tick_top()
    plt.xticks(xt, '')

    fig.suptitle('Post-fire trajectory of ' + vegty + ' burnt in ' + fire_d.strftime('%b') + fire_d.strftime('%d') + '$\pm$8 days @' + tileCord(tile), fontsize=14)
    leg = fig.legend((B, M, S), ('Burning Event', 'Mean and upper/lower 10 percentile','Sample Size'), loc='lower center', ncol=3, shadow=True, prop={'size':10})
    leg.draw_frame(False)
    #plt.tight_layout()
    plt.savefig(tile + vegty + 'EVItraj' + '.pdf', dpi=300)
    plt.close()
