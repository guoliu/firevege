regionlist = ['Boreal Forest Region','Boreal Forest Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region']
contlist = ['North America','North America','Africa','Africa','Africa','Africa','South America','South America','Africa','Africa','South America','South America']
tilelist = ['h12v03','h12v03','h20v10','h20v10','h20v09','h20v09','h13v10','h13v10','h21v08','h21v08','h13v09','h13v09']
veglist = ['greenNeedle','mixed','savanna','woodySavanna','savanna','woodySavanna','savanna','woodySavanna','greenBroad','woodySavanna','greenBroad','savanna']


for i in range(12):

print r'\begin{frame}'
print r'\frametitle{' + regionlist[i] + ', ' + contlist[i] + '}'
print r'\framesubtitle{' + tilelist[i] + ', ' + veglist[i] + '}'
print '\n'

print r'\begin{figure}[H]'
print r'\begin{center}'
print r'\includegraphics [width=1.05\textwidth]{' + tilelist[i] + veglist[i] + 'trajMap.pdf}'
print r'\end{center}'
print r'\end{figure}'
print '\n'

print r'\begin{figure}[H]'
print r'\begin{center}'
print r'\includegraphics [width=1\textwidth]{' + tilelist[i] + veglist[i] + 'EVItraj.pdf}'
print r'\end{center}'
print r'\end{figure}'
print '\n'

print r'\begin{figure}[H]'
print r'\centering'
print r'\begin{subfigure}{.53\textwidth}'
print r'\centering'
print r'\includegraphics[width=1\linewidth]{' + tilelist[i] + 'coverHis.pdf}'
print r'\end{subfigure}'
print r'\begin{subfigure}{.47\textwidth}'
print r'\centering'
print r'\includegraphics[width=1\linewidth]{' + tilelist[i] + veglist[i] + 'fireFreq.pdf}'
print r'\end{subfigure}'
print r'\end{figure}'
print '\n'

print r'\end{frame}'
print '\n'
print '\n'


