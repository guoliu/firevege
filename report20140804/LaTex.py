import collections

qua_dict = {0: 'Good',
            1: 'Marginal',
            2: 'Snow/Ice',
            3: 'Cloudy'}
    
samp_dic = {0: 'Constant',
            1: 'Variable'}

## Normal tile
#regionlist = ['Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region','Evergreen Broadleaf Forest-Savanna Region']
#contlist = ['Africa','Africa','Africa','Africa','South America','South America','Africa','Africa','South America','South America']
#tilelist = ['h20v10','h20v10','h20v09','h20v09','h13v10','h13v10','h21v08','h21v08','h13v09','h13v09']
#veglist = ['savanna','woodySavanna','savanna','woodySavanna','savanna','woodySavanna','greenBroad','woodySavanna','greenBroad','savanna']

print(r'\section{Results}') 
vegGroup = collections.OrderedDict({'Boreal Region': {'North America': {'h12v03': ('greenNeedle','openShrub'),
                                                                                                              'h11v02': ('greenNeedle','mixed')}},
                                                         'Tropical Region': {'South America': {'h12v10': ('greenBroad','savanna'),
                                                                                                               'h12v09': ('greenBroad','savanna'),
                                                                                                               'h11v10': ('greenBroad','grass')},
                                                                                      'Africa': {'h19v08': ('greenBroad','woodySavanna'),
                                                                                                   'h19v09': ('greenBroad','woodySavanna'),
                                                                                                   'h20v09': ('greenBroad','woodySavanna')}}})

for region in vegGroup.keys():
    print(r'\subsection{' + region + '}')
    for conti in vegGroup[region].keys():
        print(r'\subsubsection{' + conti + '}')
        for tile in vegGroup[region][conti].keys():
            for vegty in vegGroup[region][conti][tile]:
                print(r'\begin{frame}[fragile]')
                print(r'\frametitle{' + region + ', ' + conti + '}')
                print(r'\framesubtitle{' + tile + ', ' + vegty + '}')
                print('\n')
        
                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=1\textwidth]{' + tile + '_tree_freq_816.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')
            
                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics [width=1\textwidth]{' + tile + vegty + '_traj_000.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')
                
                print(r'\begin{figure}[H]')
                print(r'\begin{center}')
                print(r'\includegraphics[width=1\textwidth]{' + tile + vegty + '_cover_chan_0.pdf}')
                print(r'\end{center}')
                print(r'\end{figure}')
                print('\n')
            
                print(r'\end{frame}')
                print('\n')
                print('\n')

